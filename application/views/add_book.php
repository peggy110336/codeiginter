<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<title>新增書目</title>
	<style>
		h1 {
			text-align: center;
			font-weight: bold;
		}

	</style>
</head>
<body>
	<h1>輸入您的基本資料</h1>
	<div class="container">
	<form role="form">
		<div class="form-group">
			  <label class="control-label" for="studentid">學號</label>
			  <input type="text" class="form-control" id="studentid" placeholder="輸入學號">
			  
		</div>
		<div class="form-group">
			  <label class="control-label" for="name">姓名</label>
			  <input type="text" class="form-control" id="name" placeholder="輸入姓名">
			  
		</div>
		<div class="form-group">
			  <label class="control-label" for="email">信箱</label>
			  <input type="email" class="form-control" id="email" placeholder="輸入信箱">
			  
		</div>

		<button type="submit" id="databtn" class="btn btn-default">新增</button>
	</form>
		
	</div>

	<script>
	function validation(id)
	{
		if($("#"+id).val()==null || $("#"+id).val()=="")
		{
			var div = $("#"+id).closest("div");
			div.addClass("has-error");
			return false;
		}
		else
		{
			var div = $("#"+id).closest("div");
			div.removeClass("has-error");
			return true;
		}

	}
		$(document).ready(
			function()
			{
				$("#databtn").click(function()
					{
						if(!validation("studentid"))
						{
							return false;
						}
						if(!validation("name"))
						{
							return false;
						}
						if(!validation("email"))
						{
							return false;
						}
						
						$("form#dataform").submit();

					});
			}

		);
	</script>
</body>
</html>