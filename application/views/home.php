<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">	
	<title>HOME</title>
</head>
<body>
<h1>CodeIginter Page</h1>
<p>Welcome to k-peggy's home page.</p>
<div class="container">
	<div class="active tab-pane">
				<table class="table table-hover" id="table-content">
					<thead>
						<tr class="info">
							<th>活動分類</th>
							<th>活動名稱</th>
							<th>活動時間</th>
							<th>報名狀況</th>					
						</tr>
					</thead>
					<tbody>
						<tr class="success">
							<td>成果發表會</td>
							<td>行雲者期初成果發表會</td>
							<td>2014/10/02</td>
							<td class="status text-success">已報名</td>
						</tr>				
					</tbody>
				</table>
	</div>
</div>

	
</body>
</html>